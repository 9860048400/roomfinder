@extends('layouts.user_layouts.app')
@prepend('styles')

@endprepend
@section('content')


    <div class="page-heading">
        <h1>Dashboard</h1>
    </div>

    <div class="row col-12 p-3">

        <div class="col-xl-4 col-md-6 ">
            <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body bg-secondary">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">New users</h5>
                            <span class="h2 font-weight-bold mb-0">{{ $users }}</span>
                            <small>(since 30 days)</small>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                <i class="ni ni-single-02"></i>
                            </div>
                        </div>
                    </div>

                    <div>
                        <hr>
                        <h5 class="card-title text-uppercase text-muted mb-0">Total users</h5>
                        <span class="h2 font-weight-bold mb-0">{{ $total_users }}</span>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-xl-4 col-md-6 ">
            <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body bg-secondary">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">New Transactions</h5>
                            <span class="h2 font-weight-bold mb-0">{{ $trans_30 }}</span>
                            <small>(since 30 days)</small>
                            <p class="h2 font-weight-bold mb-0  text-success">Rs {{ $trans_30_price }}</p>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                <i class="ni ni-money-coins"></i>
                            </div>
                        </div>
                    </div>

                    <div>
                        <hr class="mb-2 mt-2">
                        <h5 class="card-title text-uppercase text-muted mb-0">Total Transactions</h5>
                        <span class="h2 font-weight-bold mb-0">{{ $trans_total }} </span>
                        <small>(since begining)</small>
                        <p class="h2 font-weight-bold mb-0 text-success">Rs {{ $trans_total_price }}</p>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-xl-4 col-md-6 ">
            <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body bg-secondary">
                    <div class="row">
                        <div class="col">

                            <h5 class="card-title text-uppercase text-muted mb-0">Properties</h5>
                            <span class="h2 font-weight-bold mb-0">{{ $product_30 }}</span>
                            <small>(added since 30 days)</small><br>
                            <span class="h2 font-weight-bold mb-0">{{ $product_active }}</span>
                            <small>(active)</small><br>
                            <span class="h2 font-weight-bold mb-0">{{ $product_inactive }}</span>
                            <small>(inactive)</small>

                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-pink text-white rounded-circle shadow">
                                <i class="ni ni-building"></i>
                            </div>
                        </div>
                    </div>

                    <div>
                        <hr class="mb-2 mt-2">
                        <h5 class="card-title text-uppercase text-muted mb-0">Total Properties</h5>
                        <span class="h2 font-weight-bold mb-0">{{ $product_total }}</span>
                    </div>

                </div>
            </div>
        </div>

    </div>





@endsection
