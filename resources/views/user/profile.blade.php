@extends('layouts.user_layouts.app')
@prepend('styles')

@endprepend
@section('content')


    <div class="page-heading mb-4">
        <h1>Profile <button type="button" class="ml-3 btn btn-primary btn-sm" data-bs-toggle="modal"
                data-bs-target="#exampleModal"> <i class="fa fa-edit"></i></button></h1>
    </div>

    <div class="user">
        <div class="row bg-white p-3">
            <div class="user-image col-sm-3">
                @if (auth()->user()->image)
                    <img style="width:200px;height:200px;object-fit:cover;border-radius:50%"
                        src="{{ asset('/storage/' . auth()->user()->image) }}" alt="">
                @else
                    <img style="width:200px;height:200px;object-fit:cover;border-radius:50%"
                        src="{{ asset('/assets/images/dummy_user.png') }}" alt="">
                @endif

            </div>
            <div class="user-desc col-sm-9">
                <table class="table table-striped table-bordered">
                    <tr>
                        <td>Name</td>
                        <td>{{ auth()->user()->name }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{{ auth()->user()->email }}</td>
                    </tr>
                    <tr>
                        <td>Phone Number</td>
                        <td>{{ auth()->user()->phone_number }}</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>
                            @if (auth()->user()->address)
                                {{ auth()->user()->address }} <button type="button" class="ml-3 btn btn-primary btn-sm"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal"> <i
                                        class="fa fa-edit"></i></button>
                            @else
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal"> <i class="fa fa-plus"></i> Add Address</button>
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/user/profile/edit') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="formFile" class="form-label">Profile Image</label>
                            <input class="form-control mb-2" name="image" type="file" onchange="loadFile(event)">
                            @if (auth()->user()->image)
                                <img id="output" style="width:100px;height:100px;object-fit:cover;border-radius:50%"
                                    src="{{ asset('/storage/' . auth()->user()->image) }}" alt="">
                            @else
                                <img id="output" style="width:100px;height:100px;object-fit:cover;border-radius:50%"
                                    src="{{ asset('/assets/images/dummy_user.png') }}" alt="">
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Address</label>
                            <textarea name="address" class="form-control" placeholder="Enter your address"
                                required>{{ auth()->user()->address }}</textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <script type="application/javascript">
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>






@endsection
