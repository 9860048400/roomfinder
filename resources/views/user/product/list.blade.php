@extends('layouts.user_layouts.app')
@prepend('script')

@endprepend
@section('content')

    <div class="page-heading row col-12 justify-content-between mb-4">
        <h1>Property List</h1>
        <div>
            <a href="/user/product/" class="btn btn-outline-primary btn-sm mt-3">
                ADD PROPERTY
            </a>
        </div>
    </div>

    <hr>

    <product-list isadmin="{{ $is_admin }}"></product-list>




@endsection
