@extends('layouts.user_layouts.app')
@prepend('script')

@endprepend
@section('content')

    <div class="page-heading row col-12 justify-content-between mb-4">
        <h1>Add Property</h1>
        <div>
            <a href="/user/product-list" class="btn btn-outline-primary btn-sm mt-3">
                VIEW ALL PROPERTIES
            </a>
        </div>
    </div>

    <hr>

    <product-add></product-add>




@endsection
