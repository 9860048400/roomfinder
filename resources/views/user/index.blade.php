@extends('layouts.user_layouts.app')
@prepend('styles')

@endprepend
@section('content')


    <div class="page-heading">
        <h1>Dashboard</h1>
    </div>

    <div class="row col-12 p-3">




        <div class="col-xl-4 col-md-6 ">
            <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body bg-white">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Properties</h5>
                            <span>Active:</span><span class="h2 font-weight-bold mb-0"> {{ $product_active }}</span>
                            <br>
                            <span>InActive:</span><span class="h2 font-weight-bold mb-0"> {{ $product_inactive }} </span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                <i class="ni ni-building"></i>
                            </div>
                        </div>
                    </div>

                    <div>
                        <hr class="mb-2 mt-2">
                        <h5 class="card-title text-uppercase text-muted mb-0">Total Properties</h5>
                        <span class="h2 font-weight-bold mb-0">{{ $product_total }}</span>
                    </div>

                </div>
            </div>
        </div>

    </div>





@endsection
