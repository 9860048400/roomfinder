 <!-- Topnav -->
 <nav class="navbar  p-2 navbar-top navbar-expand navbar-light bg-white  border-bottom">
     <div class="container-fluid">
         <div class="collapse navbar-collapse" id="navbarSupportedContent">
             <!-- Search form -->
             <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
                 <div class="form-group mb-0">
                     <div class="input-group input-group-alternative input-group-merge">
                         <div class="input-group-prepend">
                             <span class="input-group-text"><i class="fas fa-search"></i></span>
                         </div>
                         <input class="form-control" placeholder="Search" type="text">
                     </div>
                 </div>
                 <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main"
                     aria-label="Close">
                     <span aria-hidden="true">×</span>
                 </button>
             </form>
             <!-- Navbar links -->
             <ul class="navbar-nav align-items-center  ml-md-auto ">
                 <li class="nav-item d-xl-none">
                     <!-- Sidenav toggler -->
                     <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin"
                         data-target="#sidenav-main">
                         <div class="sidenav-toggler-inner">
                             <i class="sidenav-toggler-line"></i>
                             <i class="sidenav-toggler-line"></i>
                             <i class="sidenav-toggler-line"></i>
                         </div>
                     </div>
                 </li>
                 <li class="nav-item d-sm-none">
                     <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                         <i class="ni ni-zoom-split-in"></i>
                     </a>
                 </li>
                 <li>
                     {{-- <notification></notification> --}}
                 </li>
                 <li>
                     <a href="{{ url('/') }}" class="btn btn-success btn-sm" style="border-radius: 0px"
                         target="_blank" rel="noopener noreferrer">Open Website</a>
                 </li>

             </ul>
             <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
                 <li class="nav-item dropdown">
                     <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                         aria-expanded="false">
                         <div class="media align-items-center">
                             <span class="avatar rounded-circle bg-light">
                                 @if (auth()->user()->image)
                                     <img style="width:100%;height:100%;object-fit:cover;"
                                         src="{{ asset('/storage/' . auth()->user()->image) }}" alt="">
                                 @else
                                     <img style="width:100%;height:100%;object-fit:cover;"
                                         src="{{ asset('/assets/images/dummy_user.png') }}" alt="">
                                 @endif

                             </span>

                         </div>
                     </a>
                 </li>
                 <li class="dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                         data-bs-toggle="dropdown" aria-expanded="false">

                         <span class=" text-sm  font-weight-bold">{{ auth()->user()->name }}</span>

                     </a>
                     <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                         <li>
                             <a class="dropdown-item" href="/user/profile">
                                 <i class="ni ni-circle-08"></i> <span>Profile</span>
                             </a>
                         </li>
                         <li>
                             <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                 <i class="ni ni-user-run"></i> <span>Logout</span>
                             </a>
                         </li>


                     </ul>
                 </li>

             </ul>
         </div>
     </div>
 </nav>

 <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
     @csrf
 </form>
