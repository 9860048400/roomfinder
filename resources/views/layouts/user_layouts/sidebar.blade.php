<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs  navbar-light  bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header align-items-center">
            <a class="navbar-brand d-block col-12 m-auto" href="javascript:void(0)">
                <img src="/assets/images/logo.png" alt="">
            </a>
        </div>
        <div class="navbar-inner mt-3">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        @if (auth()->user()->is_admin == 1)
                            <a class="nav-link" href="{{ url('/admin/') }}">
                                <i class="ni ni-tv-2 text-primary"></i>
                                <span class="nav-link-text">Admin Dashboard</span>
                            </a>

                            <div class="dropdown-divider"></div>
                            <a class="nav-link" href="{{ url('/user/') }}">
                                <i class="ni ni-tv-2 text-primary"></i>
                                <span class="nav-link-text">User Dashboard</span>
                            </a>

                        @else
                            <a class="nav-link" href="{{ url('/user/') }}">
                                <i class="ni ni-tv-2 text-primary"></i>
                                <span class="nav-link-text">Dashboard</span>
                            </a>
                        @endif
                    </li>



                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="far fa-building text-primary"></i> Property
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="/user/product">Add Property</a></li>
                            <li><a class="dropdown-item" href="/user/product-list">View Property</a></li>

                        </ul>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/user/inquires') }}">
                            <i class="ni ni-notification-70 text-primary"></i>
                            <span class="nav-link-text">Property Enquires</span>
                        </a>
                    </li>

                    <div class="dropdown-divider"></div>

                    @if (auth()->user()->is_admin == 1 && auth()->user()->role_id == 1)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/transaction') }}">
                                <i class="ni ni-credit-card text-primary"></i>
                                <span class="nav-link-text">Transactions</span>
                            </a>
                        </li>
                    @endif

                    @if (auth()->user()->is_admin == 1 && auth()->user()->role_id == 1)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/users') }}">
                                <i class="ni ni-circle-08 text-primary"></i>
                                <span class="nav-link-text">Users</span>
                            </a>
                        </li>
                    @endif



                    @if (auth()->user()->is_admin == 1 && auth()->user()->role_id == 1)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/report') }}">
                                <i class="fas fa-bug text-primary"></i>
                                <span class="nav-link-text">Reports</span>
                            </a>
                        </li>
                    @endif

                    @if (auth()->user()->is_admin == 1 && auth()->user()->role_id == 1)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/contact') }}">
                                <i class="fas fa-envelope text-primary"></i>
                                <span class="nav-link-text">Contact Us</span>
                            </a>
                        </li>
                    @endif

                </ul>
            </div>
        </div>
    </div>
</nav>
