@include('layouts.user_layouts.header')
<div id="app">
    <loader></loader>
    @include('layouts.user_layouts.sidebar')

    <!-- Main content -->
    <div class="main-content" id="panel">
        @include('layouts.user_layouts.topbar')
        <main class='main-content bgc-grey-100'>

            <div class="mt-3 container-fluid">
                @yield('content')
                @include('layouts.user_layouts.footer')
            </div>
        </main>
    </div>
</div>
@include('layouts.user_layouts.script')
</body>

</html>
