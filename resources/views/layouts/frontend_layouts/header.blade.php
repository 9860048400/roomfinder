<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="{{ asset('js/frontend.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

    <link href="{{ asset('/assets/admin/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet"
        type="text/css">


    {{-- <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBkEMXezDZpWUD6XuDFLf07bao3kJq4f_Q&libraries=places&field=geometry">
    </script> --}}

    <script async="" defer=""
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsANHsHxoxSGRn9wjEN-kss974hOsFMic&amp;libraries=places">
    </script>

    @stack('script')
    @stack('styles')

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
        integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="/assets/vendors/clientjs/client.min.js"></script>

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

    <script type="application/javascript">
        function toggleNav() {
            if ($('.nav-links').css('display') == 'flex') {
                $('.nav-links').css('animation', 'nav_out 0.5s ease-in');
                setTimeout(() => {
                    $('.nav-links').css('display', 'none');
                }, 400);

            } else {
                $('.nav-links').css('display', 'flex');
                $('.nav-links').css('animation', 'nav_in 0.5s ease-in');
            }
        }

        // function navScroll(e){
        //     window.location = '/#'+e;
        // }
    </script>


</head>

<body>
