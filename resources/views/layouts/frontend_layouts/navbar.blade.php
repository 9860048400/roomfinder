{{-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand font-weight-bold" href="/">Room Finder</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/about-us">About Us</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="contact-us">Contact Us</a>
                </li>
                @if (!Auth::check())
                    <li>
                        <button style="width:150px" class="btn btn-primary" onclick="showLogin()">Login /
                            Register</button>
                        <login-register></login-register>
                    </li>
                @endif
            </ul>
        </div>

    </div>
</nav> --}}

{{-- <div class="top-notice">
    <p>Subscribe Now to get 50% off.</p>
</div> --}}
<!-- header ends -->
<div class="navbar" id="navbar">
    <div class="nav-list">

        <div class="nav-list-item">
            <div class="nav-logo">
                <img width="150px" src="{{ asset('/assets/images/logo.png') }}" class="d-block w-100" alt="...">
            </div>
        </div>

        <div class="nav-list-item" id="search-links">
            <ul class="nav-links">
                <li class="nav-item">
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/contact-us') }}">Contact Us</a>
                </li>

            </ul>
            <div class="nav-signup-login">
                @if (!auth()->check())
                    <a onclick="showLogin()">
                        <span class="btn btn-primary text-white">
                            Login / Register
                        </span>
                    </a>
                @else
                    <div class="logged-in-items">
                        <div class="fa-dashboard">
                            <i class="fas fa-user"></i>
                            {{-- <p>Account</p> --}}
                        </div>
                        <ul class="loggedin-list">
                            <li>Welcome, {{ auth()->user()->name }}</li>
                            @if (auth()->user()->is_admin == 1 && auth()->user()->role_id == 1)
                                <li><a href="/admin">Admin Dashboard</a></li>
                            @endif
                            @if (auth()->user()->is_admin == 0)
                                <li><a href="/user">My Account</a></li>
                            @endif
                            <li>
                                <a onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                    Logout <i class="fas fa-sign-out-alt"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                @endif

            </div>
        </div>
    </div>
    <div class="nav-menu">
        <i onclick="toggleNav()" class="fa fa-bars"></i>
    </div>
</div>

@if (!auth()->check())
    <login-register></login-register>
@endif

@if (auth()->check())
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endif

<main>



    <script type="application/javascript">
        function showLogin() {
            $('#loginModal').modal('show');
        }
    </script>
