@extends('layouts.frontend_layouts.app')
@prepend('styles')
    <link rel="stylesheet" href="{{ asset('/css/properties.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/swiper/css/swiper.min.css') }}">
@endprepend
@prepend('script')
    <script src="{{ asset('/assets/vendors/swiper/js/swiper.min.js') }}"></script>
@endprepend
@section('content')


    <div class="bg-light">
        <property-list loc="{{ json_encode($data) }}"></property-list>

    </div>



    <script type="application/javascript">
        function showGetDetail() {
            $('#getDetail').modal('show');
        }
    </script>




@endsection
