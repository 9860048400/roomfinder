@extends('layouts.frontend_layouts.app')
@prepend('styles')
    <link rel="stylesheet" href="/css/index.css">
@endprepend
@section('content')

    <div class="jumbotron text-center mt-5">
        <div class="icon">
            <span class="material-icons text-success" style="font-size:100px">
                check_circle_outline
            </span>

        </div>
        <h1 class="display-3">Payment Success</h1>
        <p class="lead">
            <strong>Please check your email</strong> for details.
        </p>
        <hr>
        <p>
            Having trouble? <a href="/contact-us" class="border-secondary border-bottom">Contact us</a>
        </p>
        <p class="lead">
            <a class="btn btn-primary btn-sm text-light" href="/" role="button">Go to homepage</a>
        </p>
    </div>



@endsection
