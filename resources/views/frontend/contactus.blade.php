@extends('layouts.frontend_layouts.app')
@prepend('styles')
    <link rel="stylesheet" href="/css/index.css">
@endprepend
@section('content')

    <section id="contact-head" class="pt-5 border-bottom pb-4 bg-primary">
        <div class="container">
            <h1 class="display-1 mb-3 text-light font-weight-bold">Contact Us</h1>
            <h3 class="color text-light">
                Leave us a little info, and we’ll be in touch.
            </h3>

        </div>
    </section>

    <div class="contact-body bg-light">


        <section id="contact-us" class="container">
            <div class="contact-us row pt-5 pb-5 col-12 p-0 m-0">

                <div class="contact-infos border-primary-color col-md-12 col-lg-3">
                    <div class="title-description mb-3">
                        <div class="col-12 p-0">
                            <h4 class="font-weight-bold mb-3 color">INQUIRIES</h4>
                            <p class="text-secondary color">
                                Fill in this form or send us an e-mail with your inquiry.
                            </p>
                        </div>
                    </div>
                </div>

                <section class="contact-form p-4 col-md-12 col-lg-9" id="contact-form">

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="name color">Name</label>
                            <input class="form-control mt-2" type="text" id='name' placeholder="Your Name" name="name"
                                required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="phone color">Phone</label>
                            <input class="form-control mt-2" type="number" id="phone" placeholder="Your Phone Number"
                                name="phone" required>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <label class="email color ">Email</label>
                        <input class="form-control mt-2" type="email" id="email" placeholder="Your Email Address"
                            name="email" required>
                    </div>
                    <div class="form-group mt-3">
                        <label class="message color">Message</label>
                        <textarea class="form-control mt-2" rows="5" id="message" placeholder="Message" name="message"
                            required></textarea>
                    </div>
                    <button class="btn btn-primary mt-3" onclick="sendMessage()">Send Message</button>

                    <div id="response" class="text-light bg-success mt-2">

                    </div>

                </section>

            </div>

        </section>


        <div class="contact-map col-12 m-0 p-0">
            @if (isset($map))
                @if ($map->count() !== 0)
                    {!! $map->name !!}
                @endif
            @endif
        </div>
    </div>

    <script type="application/javascript">
        function sendMessage() {
            $.ajax({
                url: '/save-contact',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    _token: "{{ csrf_token() }}",
                    'name': $('#name').val(),
                    'phone': $('#phone').val(),
                    'email': $('#email').val(),
                    'message': $('#message').val(),
                },
                success: function(response) {

                    $('#name').val(''),
                        $('#phone').val(''),
                        $('#email').val(''),
                        $('#message').val(''),
                        $('#response').html(response.message);
                    $('#response').css('padding', '5px 10px');
                    setTimeout(() => {
                        $('#response').css('padding', '0px');
                        $('#response').html('');
                    }, 4000);
                }

            })
        }
    </script>


@endsection
