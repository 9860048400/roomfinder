@extends('layouts.frontend_layouts.app')
@prepend('styles')
    <link rel="stylesheet" href="/css/index.css">
    <link rel="stylesheet" href="/css/properties.css">
@endprepend
@section('content')


    {{-- slider --}}
    <section class="slider-section">

        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{ asset('/assets/images/banner.jpg') }}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="https://images.unsplash.com/photo-1586023492125-27b2c045efd7?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=567&q=80"
                        class="d-block w-100" alt="...">
                </div>
            </div>
        </div>

        <home-search></home-search>

    </section>


    <section id="nearby">
        <home-nearby></home-nearby>
    </section>


    <section>
        <div class="top-cities mt-5 mb-5">
            <div class="title text-center mb-5">
                <h3 class="font-weight-bold">Our Top Cities</h3>
                <p class="text-secondary">Choose The City You’ll Be Living In Next, Or Look For Apartments And Rooms Near
                    You</p>
            </div>
            <div class="top-cities-list row container m-auto">
                <div class="col-xl-3 col-md-4 col-sm-6 col-12 ">
                    <a href="https://roomfinder.kaushalparajuli.com.np/properties?country=Nepal&locality=Kathmandu&route=Kathmandu"
                        class="card">
                        <img src="https://images.unsplash.com/photo-1572253765558-ed6413a68da5?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGthdGhtYW5kdXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                            class="card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">Kathmandu</h5>
                            <p class="card-text">100+ Listings Every Month</p>
                            <p class="card-text">Capital City Of Nepal</p>
                        </div>
                    </a>
                </div>


                <div class="col-xl-3 col-md-4 col-sm-6 col-12 ">
                    <a href="https://roomfinder.kaushalparajuli.com.np/properties?country=Nepal&locality=Bhaktapur&route=Bhaktapur"
                        class="card">

                        <img src="https://images.unsplash.com/photo-1572253765558-ed6413a68da5?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGthdGhtYW5kdXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                            class="card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">Bhaktapur</h5>
                            <p class="card-text">100+ Listings Every Month</p>
                            <p class="card-text">Capital City Of Nepal</p>
                        </div>
                    </a>
                </div>


                <div class="col-xl-3 col-md-4 col-sm-6 col-12 ">
                    <a href="https://roomfinder.kaushalparajuli.com.np/properties?country=Nepal&locality=Pokhara&route=Pokhara"
                        class="card">
                        <img src="https://images.unsplash.com/photo-1572253765558-ed6413a68da5?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGthdGhtYW5kdXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                            class="card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">Pokhara</h5>
                            <p class="card-text">100+ Listings Every Month</p>
                            <p class="card-text">Capital City Of Nepal</p>
                        </div>
                    </a>
                </div>


                <div class="col-xl-3 col-md-4 col-sm-6 col-12 ">
                    <a href="https://roomfinder.kaushalparajuli.com.np/properties?country=Nepal&locality=Lalitpur&route=Lalitpur"
                        class="card">
                        <img src="https://images.unsplash.com/photo-1572253765558-ed6413a68da5?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGthdGhtYW5kdXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                            class="card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">Lalitpur</h5>
                            <p class="card-text">100+ Listings Every Month</p>
                            <p class="card-text">Capital City Of Nepal</p>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </section>

    <section>
        <div class="working mb-5 container">
            <div class="text-center">
                <h3 class="font-weight-bold">How It Works</h3>
            </div>

            <div class="accordion accordion-flush col-xl-6 col-md-8 m-auto" id="accordionExample">
                <div class="accordion-item mb-4">
                    <h2 class="accordion-header " id="headingOne">
                        <button onclick="toggleWork('headingOne')"
                            class="accordion-button col-12 m-auto btn btn-outline-primary active" type="button"
                            data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true"
                            aria-controls="collapseOne">
                            Rent A Room
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body shadow mt-3 p-3 ">
                            <div class="d-flex flex-center mb-2">
                                <span class="material-icons pr-3 text-success">
                                    check_circle
                                </span>
                                <span>Fill Up A Form With The Basic Details About Your Apartment</span>
                            </div>
                            <div class="d-flex flex-center mb-2">
                                <span class="material-icons pr-3 text-success">
                                    check_circle
                                </span>
                                <span>Sign Up, Complete Your Profile And Post Your Listing For Free</span>
                            </div>
                            <div class="d-flex flex-center">
                                <span class="material-icons pr-3 text-success">
                                    check_circle
                                </span>
                                <span>That Is It! Your Listing Is Now In Front Of Thousands Of Seekers</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingTwo">
                        <button onclick="toggleWork('headingTwo')"
                            class="accordion-button col-12 m-auto collapsed btn btn-outline-primary" type="button"
                            data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false"
                            aria-controls="collapseTwo">
                            Find A Room
                        </button>
                    </h2>
                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body p-3 shadow mt-3">
                            <div class="d-flex flex-center mb-2">
                                <span class="material-icons pr-3 text-success">
                                    check_circle
                                </span>
                                <span>Search For A Locality And Find The Right Post Or A Listing</span>
                            </div>
                            <div class="d-flex flex-center mb-2">
                                <span class="material-icons pr-3 text-success">
                                    check_circle
                                </span>
                                <span>Contact The Roommate Or A Landlord To Close The Deal</span>
                            </div>
                            <div class="d-flex flex-center">
                                <span class="material-icons pr-3 text-success">
                                    check_circle
                                </span>
                                <span>That Is It! Ready To Move In?</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    </section>






    <script type="application/javascript">
        var myCarousel = document.querySelector('#carouselExampleControls')
        var carousel = new bootstrap.Carousel(myCarousel, {
            interval: 2000,
            wrap: false,
        })

        function toggleWork(name) {
            $('.accordion-button').removeClass('active');
            $('#' + name + ' button').addClass('active');
        }
    </script>

    <!-- Initialize Swiper -->
    <!-- Swiper JS -->
    <script type="application/javascript" src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script type="application/javascript">
        var swiper = new Swiper(".swiperNearby", {
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev"
            }
        });
    </script>

@endsection
