<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Info Mail</title>

    <style>
        * {
            font-family: arial;
        }

        body {
            background-color: #f8f9fe;

        }

        .container {
            width: 70%;
            margin: auto;
            background-color: #fff;
            border: 1px solid grey;
        }

        .logo {
            padding: 20px;
        }

        .logo img {
            width: 250px;
            margin: auto;
        }

        table {
            width: 100%;

        }

        table tr {
            border-bottom: 1px solid #dddddd;
        }

        table tr td {
            padding: 10px;
            border-bottom: 1px solid #dddddd;
            text-align: center;
        }

        .t-title {
            background-color: #2a6efd;
            color: white;
            font-weight: bold;
            letter-spacing: 1px;
        }

        .user-info {
            padding: 30px
        }

    </style>

</head>

<body>

    <div class="container">
        <div class="logo">
            <img src="/assets/images/logo.png" alt="">
        </div>
        <hr>
        <h1 style="padding-left:30px;text-align:center;color:green">Payment Successfull !!</h1>
        <div class="user-info">
            <p>
                Dear {{ $data['details']->name }},
            </p>
            <p>
                Greetings
            </p>
            <p>
                Thank you for using our service.
            </p>

            <br>
            <h3>INVOICE</h3>
            <table border="0" cellspacing="0">
                <tr class="t-title">
                    <td>Date</td>
                    <td>Name</td>
                    <td>Mobile Number</td>
                    <td>Amount</td>

                </tr>
                <tr>
                    <td>{{ $data['date'] }}</td>
                    <td>{{ $data['details']->name }}</td>
                    <td>{{ $data['details']->phone_number }}</td>
                    <td>Rs {{ $data->price }}</td>
                </tr>

            </table>
        </div>
    </div>

</body>

</html>
