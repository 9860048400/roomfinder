<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Info Mail</title>

    <style>
        * {
            font-family: arial;
        }

        body {
            background-color: #f8f9fe;

        }

        .container {
            width: 70%;
            margin: auto;
            background-color: #fff;
            border: 1px solid grey;
        }

        .logo {
            padding: 20px;
        }

        .logo img {
            width: 250px;
            margin: auto;
        }

        table {
            width: 100%;

        }

        table tr {
            border-bottom: 1px solid #dddddd;
        }

        table tr td {
            padding: 10px;
            border-bottom: 1px solid #dddddd;
        }

        .t-title {
            background-color: #2a6efd;
            color: white;
            font-weight: bold;
            letter-spacing: 1px;
        }

    </style>

</head>

<body>

    <div class="container">
        <div class="logo">
            <img src="/assets/images/logo.png" alt="">
        </div>
        <div class="user-info">
            <table border="0" cellspacing="0">
                <tr class="t-title">
                    <td>Owner Information</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>{{ $product['user']['name'] }}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{{ $product['user']['email'] }}</td>
                </tr>
                <tr>
                    <td>Mobile Number</td>
                    <td>{{ $product['user']['phone_number'] }}</td>
                </tr>
                <tr>
                    <td>Location</td>
                    <td>{{ $product['detail']['country'] }}</td>
                </tr>
                <tr class="t-title">
                    <td>Propety Information</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Property Title</td>
                    <td>{{ $product['title'] }}</td>
                </tr>
                <tr>
                    <td>Number Of Floors</td>
                    <td>{{ $product['detail']['floor'] }}</td>
                </tr>
                <tr>
                    <td>Number Of Rooms</td>
                    <td>{{ $product['detail']['rooms'] }}</td>
                </tr>
                <tr>
                    <td>Facilities</td>
                    <td>{{ $product['detail']['facilities'] }}</td>
                </tr>
                <tr>
                    <td>Furnishing</td>
                    <td>{{ $product['detail']['furnishing'] }}</td>
                </tr>
                <tr>
                    <td>Access Road</td>
                    <td>{{ $product['detail']['access_road'] }}</td>
                </tr>
                <tr>
                    <td>Building Area</td>
                    <td>{{ $product['detail']['building_area'] }}</td>
                </tr>
                <tr>
                    <td>Land Area</td>
                    <td>{{ $product['detail']['land_area'] }}</td>

                </tr>
                <tr class="price">
                    <td>Price</td>
                    <td>{{ $product['detail']['price'] }}</td>
                </tr>
            </table>
        </div>
    </div>

</body>

</html>
