import axios from "axios";

const state = {
    users: [],
    pagination: {
        currentPage: 1,
        perPage: 5,
        total: 0,
        totalPage: 0
    }
};

const getters = {
    allUsers: state => state.users,
    usersPagination: state => state.pagination
};

const actions = {
    async fetechAllUsers({ commit }, payload) {
        try {
            const res = await axios.post(
                `/admin/get-all-users?${payload.page}`,
                payload
            );
            commit("SET_USERS", res.data);
            console.log(res.data);
        } catch (error) {
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    },

    async lockUser({ commit }, payload) {
        try {
            const res = await axios.get(`/admin/user/lock/` + payload.id);
            console.log(res.data);
            commit("LOCK_USERS", res.data);
            if (res.data.is_locked == 1) {
                Vue.$toast.open("User Locked  Successfully");
            } else {
                Vue.$toast.open("User Unlocked  Successfully");
            }
        } catch (error) {
            console.log(error.message);
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    }
};

const mutations = {
    SET_USERS(state, users) {
        (state.users = users.data),
            (state.pagination = {
                currentPage: users.current_page,
                perPage: users.per_page,
                total: users.total,
                totalPage: users.last_page
            });
    },
    LOCK_USERS(state, payload) {
        const idx = state.users.findIndex(u => u.id === payload.id);

        if (idx >= 0) {
            Vue.set(state.users, idx, payload);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
