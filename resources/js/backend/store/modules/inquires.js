import axios from "axios";

const state = {
    inquires: [],
    pagination: {
        currentPage: 1,
        perPage: 5,
        total: 0,
        totalPage: 0
    }
};

const getters = {
    allInquires: state => state.inquires,
    inquiresPagination: state => state.pagination
};

const actions = {
    async fetchInquires({ commit }, payload) {
        try {
            const res = await axios.post(
                `/user/get-all-inquires?${payload.page}`,
                payload
            );
            commit("SET_INQUIRES", res.data);
            console.log(res.data);
        } catch (error) {
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    },

    async sendReport({ commit }, payload) {
        console.log(payload);
        try {
            var res = await axios.post("/user/report", payload);
            commit("SEND_REPORT", res.data);
            Vue.$toast.open("User Reported Successfully");
        } catch (error) {
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
            console.log(error.message);
        }
    }
};

const mutations = {
    SET_INQUIRES(state, inquires) {
        (state.inquires = inquires.data),
            (state.pagination = {
                currentPage: inquires.current_page,
                perPage: inquires.per_page,
                total: inquires.total,
                totalPage: inquires.last_page
            });
    },
    SEND_REPORT(state, payload) {
        const idx = state.inquires.findIndex(u => u.id === payload.id);

        if (idx >= 0) {
            Vue.set(state.inquires, idx, payload);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
