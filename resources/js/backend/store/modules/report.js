import axios from "axios";

const state = {
    reports: [],
    pagination: {
        currentPage: 1,
        perPage: 5,
        total: 0,
        totalPage: 0
    }
};

const getters = {
    allReports: state => state.reports,
    reportPagination: state => state.pagination
};

const actions = {
    async fetchAllReports({ commit }, payload) {
        try {
            const res = await axios.post(
                `/admin/get-all-reports?${payload.page}`,
                payload
            );
            commit("SET_REPORTS", res.data);
            console.log(res.data);
        } catch (error) {
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    },

    async approveReport({ commit }, payload) {
        try {
            const res = await axios.get(`/admin/report/approve/` + payload.id);
            console.log(res.data);
            commit("APPROVE_REPORTS", res.data);
            if (res.data.is_approved == 1) {
                Vue.$toast.open("Report Approved  Successfully");
            } else {
                Vue.$toast.open("Report Unapproved  Successfully");
            }
        } catch (error) {
            console.log(error.message);
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    }
};

const mutations = {
    SET_REPORTS(state, reports) {
        (state.reports = reports.data),
            (state.pagination = {
                currentPage: reports.current_page,
                perPage: reports.per_page,
                total: reports.total,
                totalPage: reports.last_page
            });
    },
    APPROVE_REPORTS(state, payload) {
        const idx = state.reports.findIndex(u => u.id === payload.id);

        if (idx >= 0) {
            Vue.set(state.reports, idx, payload);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
