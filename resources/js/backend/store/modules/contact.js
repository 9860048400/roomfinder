import axios from "axios";

const state = {
    contacts: [],
    pagination: {
        currentPage: 1,
        perPage: 5,
        total: 0,
        totalPage: 0
    }
};

const getters = {
    allContacts: state => state.contacts,
    contactPagination: state => state.pagination
};

const actions = {
    async fetchAllContacts({ commit }, payload) {
        try {
            const res = await axios.post(
                `/admin/get-all-contacts?${payload.page}`,
                payload
            );
            commit("SET_CONTACTS", res.data);
            console.log(res.data);
        } catch (error) {
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    },

    async approveContact({ commit }, payload) {
        try {
            const res = await axios.get(`/admin/contact/approve/` + payload.id);
            console.log(res.data);
            commit("APPROVE_REPORTS", res.data);

            Vue.$toast.open("Updated  Successfully");
        } catch (error) {
            console.log(error.message);
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    }
};

const mutations = {
    SET_CONTACTS(state, contacts) {
        (state.contacts = contacts.data),
            (state.pagination = {
                currentPage: contacts.current_page,
                perPage: contacts.per_page,
                total: contacts.total,
                totalPage: contacts.last_page
            });
    },
    APPROVE_REPORTS(state, payload) {
        const idx = state.contacts.findIndex(u => u.id === payload.id);

        if (idx >= 0) {
            Vue.set(state.contacts, idx, payload);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
