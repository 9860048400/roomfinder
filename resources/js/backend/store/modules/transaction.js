import axios from "axios";
import global from "./global";

const state = {
    transactions: [],
    pagination: {
        currentPage: 1,
        perPage: 5,
        total: 0,
        totalPage: 0
    }
};

const getters = {
    allTransactions: state => state.transactions,
    transactionPagination: state => state.pagination
};

const actions = {
    async fetchTransactions({ commit }, payload) {
        try {
            const res = await axios.post(
                `/admin/get-all-transaction?${payload.page}`,
                payload
            );
            commit("SET_TRANSACTIONS", res.data);
            console.log(res.data);
        } catch (error) {
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    },

    async deleteTransaction({ commit }, payload) {
        try {
            const res = await axios.delete(`/admin/product/` + payload.id);
            console.log(res);
            commit("DELETE_TRANSACTIONS", payload.id);
            Vue.$toast.open("Item Deleted Successfully");
        } catch (error) {
            console.log(error.message);
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    },

    async sendMail({ commit }, payload) {
        commit("SET_LOADING", true);
        try {
            const res = await axios.get(`/admin/send-mail/` + payload.id);
            console.log(res);
            commit("SET_LOADING", false);
            Vue.$toast.open("Mail Sent  Successfully");
        } catch (error) {
            commit("SET_LOADING", false);
            console.log(error.message);
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    }
};

const mutations = {
    SET_TRANSACTIONS(state, transactions) {
        (state.transactions = transactions.data),
            (state.pagination = {
                currentPage: transactions.current_page,
                perPage: transactions.per_page,
                total: transactions.total,
                totalPage: transactions.last_page
            });
    },
    DELETE_TRANSACTIONS(state, id) {
        state.transactions = state.transactions.filter(
            transactions => transactions.id !== id
        );
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
