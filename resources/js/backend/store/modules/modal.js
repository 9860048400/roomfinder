const state = {
    state: true,
    message: ""
};

const getters = {
    getModalState: state => state.state,
    getMessageModal: state => state.message
};

const actions = {
    loadMesgModal({ commit }, payload) {
        commit("SET_MODAL_STATE", payload.status);
        commit("SET_MODAL_MESSAGE", payload.mesg);
    }
};

const mutations = {
    SET_MODAL_MESSAGE(state, payload) {
        state.message = payload;
    },
    SET_MODAL_STATE(state, payload) {
        state.state = payload;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
