import axios from "axios";

const state = {
    products: [],
    pagination: {
        currentPage: 1,
        perPage: 5,
        totalProducts: 0,
        totalPage: 0
    }
};

const getters = {
    allProducts: state => state.products,
    pagination: state => state.pagination
};

const actions = {
    async fetchProducts({ commit }, payload) {
        try {
            const res = await axios.post(
                `/user/get-all-product?${payload.page}`,
                payload
            );
            commit("SET_PRODUCTS", res.data);
            console.log(res.data);
        } catch (error) {
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    },

    async approveProduct({ commit }, payload) {
        try {
            const res = await axios.get("/admin/product/approve/" + payload.id);
            commit("APPROVE_PRODUCT", res.data);
            if (res.data.is_approved == 1) {
                Vue.$toast.open("Product Approved  Successfully");
            } else {
                Vue.$toast.open("Product Unapproved  Successfully");
            }
        } catch (error) {
            console.log(error.message);

            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    },

    async deleteProduct({ commit }, payload) {
        try {
            const res = await axios.delete(`/user/product/` + payload.id);
            console.log(res);
            commit("DELETE_PRODUCT", payload.id);
            Vue.$toast.open("Item Deleted Successfully");
        } catch (error) {
            console.log(error.message);
            Vue.$toast.open({
                type: "error",
                message: "Something went wrong"
            });
        }
    }
};

const mutations = {
    SET_PRODUCTS(state, products) {
        (state.products = products.data),
            (state.pagination = {
                currentPage: products.current_page,
                perPage: products.per_page,
                totalProducts: products.total,
                totalPage: products.last_page
            });
    },
    DELETE_PRODUCT(state, id) {
        state.products = state.products.filter(products => products.id !== id);
    },
    APPROVE_PRODUCT(state, payload) {
        const idx = state.products.findIndex(u => u.id === payload.id);

        if (idx >= 0) {
            Vue.set(state.products, idx, payload);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
