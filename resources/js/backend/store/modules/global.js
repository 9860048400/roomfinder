const state = {
    loading: false
};

const getters = {
    setLoading: state => state.loading
};

const mutations = {
    SET_LOADING(state, payload) {
        state.loading = payload;
    }
};

export default {
    state,
    getters,
    mutations
};
