import Vuex from "vuex";
import Vue from "vue";

import product from "./modules/product";
import transaction from "./modules/transaction";
import users from "./modules/users";
import inquires from "./modules/inquires";
import modal from "./modules/modal";
import report from "./modules/report";
import global from "./modules/global";
import contact from "./modules/contact";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        global,
        contact,
        report,
        product,
        modal,
        inquires,
        users,
        transaction
    }
});
