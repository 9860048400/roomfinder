require("./bootstrap");
window.Vue = require("vue").default;

Vue.component("loader", require("./components/loader").default);
Vue.component("product-add", require("./backend/user/product/Add").default);
Vue.component("product-list", require("./backend/user/product/List").default);

// admin
Vue.component(
    "transaction",
    require("./backend/admin/transaction/List").default
);
Vue.component("users", require("./backend/admin/users/List").default);
Vue.component("enquires", require("./backend/user/enquires/Index").default);
Vue.component("report", require("./backend/admin/report/Index").default);
Vue.component("contact", require("./backend/admin/contact/Index").default);

// custom global

Vue.component("message-modal", require("./components/mesgModal").default);

// global components
import Notifications from "vue-notification";
Vue.use(Notifications);

import { BTable, BPagination, BootstrapVue } from "bootstrap-vue";
Vue.component("b-table", BTable);
Vue.component("b-pagination", BPagination);
Vue.use(BootstrapVue);

Vue.use(require("vue-moment"));

import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/theme-sugar.css";
Vue.use(VueToast);

import store from "./backend/store";

// Vue.mixin({
//     data: function() {
//         return { isLoading: false };
//     }
// });

window.onload = () => {
    const app = new Vue({
        el: "#app",
        store
    });
};
