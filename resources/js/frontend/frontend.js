require("../bootstrap");
window.Vue = require("vue").default;

Vue.component("loader", require("../components/loader").default);
Vue.component(
    "login-register",
    require("../components/login-register").default
);
Vue.component("home-search", require("./components/HomeSearch").default);
Vue.component("property-list", require("./properties/PropertyList").default);
Vue.component("home-nearby", require("./components/HomeNearby").default);

const app = new Vue({
    el: "#app"
});
