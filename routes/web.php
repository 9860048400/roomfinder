<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Auth::routes();

Route::get('/', [App\Http\Controllers\Frontend\FrontendController::class, 'index'])->name('home');
Route::get('/properties', [App\Http\Controllers\Frontend\Property\PropertyController::class, 'index']);
Route::post('/get-location-data', [App\Http\Controllers\Frontend\Property\PropertyController::class, 'getLocationData']);
Route::post('/get-nearby-data', [App\Http\Controllers\Frontend\Property\PropertyController::class, 'getNearbyData']);

Route::get('/contact-us', [App\Http\Controllers\Frontend\FrontendController::class, 'contactus'])->name('contactus');
Route::post('/save-contact', [App\Http\Controllers\Frontend\FrontendController::class, 'saveContact']);

Route::post('/send-otp', [App\Http\Controllers\Payment\OtpController::class, 'sendOTP']);
Route::post('/verify-otp', [App\Http\Controllers\Payment\OtpController::class, 'verifyOTP']);

Route::post('/khalti/payment/verify', [App\Http\Controllers\Payment\PaymentController::class, 'verifyKhaltiPayment']);
Route::post('/payment/save-session', [App\Http\Controllers\Payment\PaymentController::class, 'savePaymentSession']);
Route::get('/payment/success', [App\Http\Controllers\Frontend\FrontendController::class, 'paymentSuccess']);
Route::get('/view-count/{product_id}', [App\Http\Controllers\Frontend\FrontendController::class, 'productCount']);