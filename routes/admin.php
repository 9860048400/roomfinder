<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function () {

    Route::get('/', [App\Http\Controllers\Backend\DashboardController::class, 'index']);
    Route::get('/transaction', [App\Http\Controllers\Backend\TransactionController::class, 'index']);
    Route::post('/get-all-transaction', [App\Http\Controllers\Backend\TransactionController::class, 'getAllTransaction']);
    Route::get('/send-mail/{invoice_id}', [App\Http\Controllers\Backend\TransactionController::class, 'sendMail']);
    Route::get('/users', [App\Http\Controllers\Backend\UserController::class, 'index']);
    Route::post('/get-all-users', [App\Http\Controllers\Backend\UserController::class, 'getAllUsers']);
    Route::get('/user/lock/{id}', [App\Http\Controllers\Backend\UserController::class, 'lockUser']);
    Route::get('/report', [App\Http\Controllers\Backend\ReportController::class, 'index']);
    Route::post('/get-all-reports', [App\Http\Controllers\Backend\ReportController::class, 'getAllReports']);
    Route::get('/report/approve/{id}', [App\Http\Controllers\Backend\ReportController::class, 'ApproveReport']);

    Route::get('/contact', [App\Http\Controllers\Backend\ContactController::class, 'index']);
    Route::post('/get-all-contacts', [App\Http\Controllers\Backend\ContactController::class, 'getAllContacts']);
    Route::get('/contact/approve/{id}', [App\Http\Controllers\Backend\ContactController::class, 'ApproveContact']);
    Route::get('/product/approve/{id}', [App\Http\Controllers\User\Product\ProductController::class, 'ApproveProduct']);

});