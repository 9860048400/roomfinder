<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'user', 'prefix' => 'user'], function () {

    Route::get('/', [App\Http\Controllers\User\DashboardController::class, 'index']);
    Route::resource('/product', App\Http\Controllers\User\Product\ProductController::class);
    Route::post('/edit/product', [App\Http\Controllers\User\Product\ProductController::class, 'update']);
    Route::get('/product-list', [App\Http\Controllers\User\Product\ProductController::class, 'productList']);
    Route::post('/get-all-product', [App\Http\Controllers\User\Product\ProductController::class, 'getAll']);
    Route::get('/profile', [App\Http\Controllers\User\DashboardController::class, 'profile']);
    Route::post('/profile/edit', [App\Http\Controllers\User\DashboardController::class, 'editProfile']);

    Route::get('/inquires', [App\Http\Controllers\User\UserEnquiryController::class, 'index']);
    Route::post('/get-all-inquires', [App\Http\Controllers\User\UserEnquiryController::class, 'getAllInquires']);
    Route::post('/report', [App\Http\Controllers\User\UserEnquiryController::class, 'reportUser']);

});