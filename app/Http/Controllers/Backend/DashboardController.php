<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::where('created_at', '>', Carbon::now()->subDays(30))->count();
        $total_users = User::all()->count();

        $trans_30 = Invoice::where('created_at', '>', Carbon::now()->subDays(30))->count();
        $trans_30_price = Invoice::where('created_at', '>', Carbon::now()->subDays(30))->sum('price');

        $trans_total = Invoice::all()->count();
        // dd($trans_total);

        $trans_total_price = Invoice::sum('price');

        $product_total = Product::all()->count();

        $product_active = Product::where('status', 1)->count();
        $product_inactive = Product::where('status', 0)->count();
        $product_30 = Product::where('created_at', '>', Carbon::now()->subDays(30))->count();

        return view('backend.index', [
            'users' => $users,
            'total_users' => $total_users,
            'trans_30' => $trans_30,
            'trans_30_price' => $trans_30_price,
            'trans_total' => $trans_total,
            'trans_total_price' => $trans_total_price,
            'product_active' => $product_active,
            'product_inactive' => $product_inactive,
            'product_total' => $product_total,
            'product_30' => $product_30,
        ]);
    }
}