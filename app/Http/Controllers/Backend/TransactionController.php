<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Jobs\SendInfoMail;
use App\Models\Invoice;
use App\Models\Product;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index()
    {
        return view('backend.transaction');
    }

    public function getAllTransaction(Request $request)
    {

        // dd($request);
        if ($request->searchBy == 'phone_number') {
            $data = Invoice::with('details', 'property')
                ->whereHas('details', function ($q) use ($request) {
                    $q->where('phone_number', 'LIKE', '%' . $request->keyword);
                })
                ->latest()
                ->paginate(10);
        } else {
            if ($request->sort_by == 'success') {
                $data = Invoice::with('details', 'property')
                    ->where($request->searchBy, 'LIKE', '%' . $request->keyword)
                    ->where('status', 1)
                    ->latest()
                    ->paginate(10);
            } elseif ($request->sort_by == 'failed') {
                $data = Invoice::with('details', 'property')
                    ->where($request->searchBy, 'LIKE', '%' . $request->keyword)
                    ->where('status', 0)
                    ->latest()
                    ->paginate(10);
            } else {
                dd(true);
                $data = Invoice::with('details', 'property')
                    ->where($request->searchBy, 'LIKE', '%' . $request->keyword)
                    ->latest()
                    ->paginate(10);

            }

        }
        return json_encode($data);

    }

    public function sendMail($id)
    {

        try {
            $invoice = Invoice::where('id', $id)->with('details')->first();
            $data = [
                'product' => Product::where('id', $invoice->product_id)->with('detail', 'user')->first(),
                'invoice' => $invoice,
            ];

            dispatch(new SendInfoMail($data));

            return true;

        } catch (\Throwable $th) {
            throw $th;
        }

    }

}