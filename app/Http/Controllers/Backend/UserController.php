<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return view('backend.users');
    }

    public function getAllUsers(Request $request)
    {

        // dd($request);
        if ($request->sort_by == 'locked') {
            $data = User::where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->where('is_locked', 1)
                ->latest()
                ->paginate(10);
        } elseif ($request->sort_by == 'admin') {
            $data = User::where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->where('is_admin', 1)
                ->latest()
                ->paginate(10);
        } else {
            $data = User::where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->latest()
                ->paginate(10);

        }
        return json_encode($data);

    }

    public function lockUser($id)
    {
        try {
            $user = User::find($id);
            if ($user->is_locked == 0) {
                $user->is_locked = 1;
            } else {
                $user->is_locked = 0;
            }
            $user->update();

            return $user;
        } catch (\Exception $e) {
            throw response()->json('Something went wrong');
        }

    }

}