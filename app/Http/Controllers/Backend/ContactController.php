<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        return view('backend.contact');
    }

    public function getAllContacts(Request $request)
    {

        // dd($request);
        if ($request->sort_by == 'seen') {
            $data = Contact::where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->where('is_seen', 1)
                ->latest()
                ->paginate(10);
        } elseif ($request->sort_by == 'notseen') {
            $data = Contact::where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->where('is_seen', 0)
                ->latest()
                ->paginate(10);
        } else {
            $data = Contact::where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->latest()
                ->paginate(10);

        }
        return json_encode($data);

    }

    public function ApproveContact($id)
    {
        try {
            $report = Contact::find($id);
            if ($report->is_seen == 0) {
                $report->is_seen = 1;
            } else {
                $report->is_seen = 0;
            }
            $report->update();

            return $report;
        } catch (\Exception $e) {
            throw response()->json('Something went wrong');
        }

    }
}