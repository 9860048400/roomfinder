<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ReportUser;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index()
    {
        return view('backend.report');
    }

    public function getAllReports(Request $request)
    {
        $request->searchBy = 'report_users.' . $request->searchBy;

        // dd($request);
        if ($request->sort_by == 'approved') {
            $data = ReportUser::join('invoices', 'invoices.id', 'report_users.invoice_id')
                ->join('products', 'products.id', 'invoices.product_id')
                ->join('users', 'users.id', 'products.user_id')
                ->select('report_users.id', 'report_users.message', 'report_users.phone_number as abuser',
                    'report_users.message', 'report_users.created_at', 'report_users.created_at', 'report_users.is_approved',
                    'users.name', 'users.email', 'users.phone_number'
                )
                ->where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->where('report_users.is_approved', 1)
                ->orderBy('report_users.id', 'desc')
                ->paginate(10);
        } elseif ($request->sort_by == 'notapproved') {
            $data = ReportUser::join('invoices', 'invoices.id', 'report_users.invoice_id')
                ->join('products', 'products.id', 'invoices.product_id')
                ->join('users', 'users.id', 'products.user_id')
                ->select('report_users.id', 'report_users.message', 'report_users.phone_number as abuser',
                    'report_users.message', 'report_users.created_at', 'report_users.created_at', 'report_users.is_approved',
                    'users.name', 'users.email', 'users.phone_number'
                )
                ->where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->where('report_users.is_approved', 0)
                ->orderBy('report_users.id', 'desc')
                ->paginate(10);
        } else {
            $data = ReportUser::where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->join('invoices', 'invoices.id', 'report_users.invoice_id')
                ->join('products', 'products.id', 'invoices.product_id')
                ->join('users', 'users.id', 'products.user_id')
                ->select('report_users.id', 'report_users.message', 'report_users.phone_number as abuser',
                    'report_users.message', 'report_users.created_at', 'report_users.created_at', 'report_users.is_approved',
                    'users.name', 'users.email', 'users.phone_number'
                )
                ->orderBy('report_users.id', 'desc')
                ->paginate(10);

        }
        return json_encode($data);

    }

    public function ApproveReport($id)
    {
        try {
            $report = ReportUser::find($id);
            if ($report->is_approved == 0) {
                $report->is_approved = 1;
            } else {
                $report->is_approved = 0;
            }
            $report->update();

            return $report;
        } catch (\Exception $e) {
            throw response()->json('Something went wrong');
        }

    }
}