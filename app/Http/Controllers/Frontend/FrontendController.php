<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\BaseController;
use App\Models\Contact;
use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends BaseController
{
    public function index()
    {
        return view('frontend.index');
    }

    public function paymentSuccess()
    {
        return view('frontend.payment.payment-success');
    }

    public function contactus()
    {
        return view('frontend.contactus');
    }

    public function productCount($id)
    {
        $p = Product::find($id);
        $p->views = $p->views + 1;
        $p->update();
        return 'success';
    }

    // saving contact details --------------------------
    public function saveContact(Request $request)
    {
        // dd($request->all());
        $validate = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'message' => 'required|string',
        ]);

        $store = new Contact();
        $store->name = $request->name;
        $store->phone = $request->phone;
        $store->email = $request->email;
        $store->message = $request->message;
        $store->save();
        if ($store) {
            return json_encode($this->reportSuccess('Message sent Successfully. Our team will contact you soon.'));

        } else {
            return json_encode($this->reportError('Message send failed. Please try again'));
        }

    }

}