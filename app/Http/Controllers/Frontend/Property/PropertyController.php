<?php

namespace App\Http\Controllers\Frontend\Property;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Stevebauman\Location\Facades\Location;

class PropertyController extends Controller
{
    public function index()
    {
        // dd($_GET);
        $data['country'] = $_GET['country'];
        $data['locality'] = $_GET['locality'];
        $data['route'] = $_GET['route'];

        return view('frontend.properties.properties', [
            'data' => $data,
        ]);
    }

    public function getLocationData(Request $request)
    {
        // dd($request->all());
        try {
            $product = Product::join('product_details', 'product_details.product_id', 'products.id')
                ->where(function ($q) use ($request) {

                    if ($request->locality !== null) {
                        $q->orWhere('product_details.locality', 'LIKE', '%' . $request->locality . '%');
                    }
                    if ($request->route != null) {
                        $q->orWhere('product_details.route', 'LIKE', '%' . $request->route . '%');
                    }
                })
                ->where(function ($q) use ($request) {
                    if ($request->price) {
                        $price = explode('-', $request->price);
                        $q->where('product_details.price', '>', $price[0])->where('product_details.price', '<', $price[1]);
                    }
                    if ($request->furnishing) {
                        $q->where('product_details.furnishing', $request->furnishing);
                    }
                })
                ->select('products.*', 'product_details.ad_type', 'product_details.furnishing', 'product_details.property_face', 'product_details.access_road', 'product_details.building_area', 'product_details.land_area', 'product_details.floor', 'product_details.facilities', 'product_details.country', 'product_details.locality', 'product_details.route', 'product_details.price', 'product_details.longitude', 'product_details.latitude')
                ->where('products.status', 1)
                ->where('products.is_approved', 1)
                ->get();

            return json_encode($product);
        } catch (\Throwable $th) {
            throw $th;
        }

    }

    public function getNearbyData(Request $request)
    {

        // dd($request->all());
        $ip = $this->get_client_ip(); //Dynamic IP address get
        $data = Location::get($ip);
        // $data = $this->get_client_ip();

        $latitude = $request->latitude;
        $longitude = $request->longitude;

        try {

            if ($latitude & $longitude) {

                $product = $this->getNearbyLocation($latitude, $longitude, 5);

                if ($product && count($product) == 0) {
                    $product = $this->getNearbyLocation($latitude, $longitude, 5);
                    if ($product && count($product) == 0) {
                        $product = $this->getNearbyLocation($latitude, $longitude, 10);
                        if ($product && count($product) == 0) {
                            $product = $this->getNearbyLocation($latitude, $longitude, 20);
                            if ($product && count($product) == 0) {
                                $product = $this->getNearbyLocation($latitude, $longitude, 50);
                                if ($product && count($product) == 0) {
                                    $product = $this->getNearbyLocation($latitude, $longitude, 100);
                                    if ($product && count($product) == 0) {
                                        $product = $this->getNearbyLocation($latitude, $longitude, 500);
                                    }
                                }
                            }

                        }

                    }

                }

            } else {

                $product = Product::join('product_details', 'product_details.product_id', 'products.id')
                    ->where('products.status', 1)
                    ->take(9)
                    ->where('products.is_approved', 1)
                    ->latest()
                    ->get();

            }

            return json_encode($product);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    protected function getNearbyLocation($latitude, $longitude, $radius)
    {
        $product = Product::join('product_details', 'product_details.product_id', 'products.id')
            ->select(DB::raw('*,( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( latitude ) ) ) ) AS distance'))
            ->having('distance', '<', $radius) //in kilometer
            ->where('products.status', 1)
            ->orderBy('distance')
            ->take(9)
            ->where('products.is_approved', 1)
            ->get();
        return $product;
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } else if (isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

}