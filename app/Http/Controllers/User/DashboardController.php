<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    public function index()
    {
        $product_total = Product::where('user_id', auth()->user()->id)->count();
        $product_active = Product::where('user_id', auth()->user()->id)->where('status', 1)->count();
        $product_inactive = Product::where('user_id', auth()->user()->id)->where('status', 0)->count();

        return view('user.index', [
            'product_active' => $product_active,
            'product_inactive' => $product_inactive,
            'product_total' => $product_total,
        ]);

    }
    public function profile()
    {
        return view('user.profile');
    }

    public function editProfile(Request $request)
    {
        // dd($request->all());
        $validation = $request->validate([
            'address' => 'required|string',
        ]);
        try {
            if ($request->hasFile('image')) {
                $image = Storage::disk('public')->put('users', $request->image);
            }
            $user = User::find(auth()->user()->id);
            if (isset($image)) {
                $user->image = $image;
            }
            $user->address = $request->address;
            $user->update();
            return redirect()->back()->with('success', 'Data Updated Successfully.');

        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Something went wrong.');
        }
    }
}