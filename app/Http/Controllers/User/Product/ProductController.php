<?php

namespace App\Http\Controllers\User\Product;

use App\Http\Controllers\BaseController;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.product.add');
    }

    public function productList()
    {
        $is_admin = auth()->user()->is_admin;
        return view('user.product.list', ['is_admin' => $is_admin]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = (new Product)->where('id', $id)->with('detail')->first();
        return view('user.product.edit', [
            'product' => $product,
        ]);

    }

    public function getAll(Request $request)
    {

        if (auth()->user()->is_admin == 1) {

            if ($request->sort_by == 'active') {
                $data = Product::with('detail')
                    ->where($request->search_col, 'LIKE', "%" . $request->keyword . "%")
                    ->where('status', 1)->latest()->paginate(5);
            } elseif ($request->sort_by == 'inactive') {
                $data = Product::with('detail')
                    ->where($request->search_col, 'LIKE', "%" . $request->keyword . "%")
                    ->where('status', 0)
                    ->latest()->paginate(5);
            } elseif ($request->sort_by == 'admin') {
                $data = Product::where('user_id', Auth::user()->id)->with('detail')
                    ->where($request->search_col, 'LIKE', "%" . $request->keyword . "%")
                    ->latest()->paginate(5);
            } elseif ($request->sort_by == 'approved') {
                $data = Product::with('detail')
                    ->where($request->search_col, 'LIKE', "%" . $request->keyword . "%")
                    ->where('is_approved', 1)
                    ->latest()->paginate(5);
            } elseif ($request->sort_by == 'notapproved') {
                $data = Product::with('detail')
                    ->where($request->search_col, 'LIKE', "%" . $request->keyword . "%")
                    ->where('is_approved', 0)
                    ->latest()->paginate(5);
            } else {
                $data = Product::with('detail')
                    ->where($request->search_col, 'LIKE', "%" . $request->keyword . "%")
                    ->latest()->paginate(5);
            }

        } else {
            if ($request->sort_by == 'active') {
                $data = Product::where('user_id', Auth::user()->id)->with('detail')
                    ->where($request->search_col, 'LIKE', "%" . $request->keyword . "%")
                    ->where('status', 1)->latest()->paginate(5);
            } elseif ($request->sort_by == 'inactive') {
                $data = Product::where('user_id', Auth::user()->id)->with('detail')
                    ->where($request->search_col, 'LIKE', "%" . $request->keyword . "%")
                    ->where('status', 0)
                    ->latest()->paginate(5);
            } elseif ($request->sort_by == 'approved') {
                $data = Product::where('user_id', Auth::user()->id)->with('detail')
                    ->where($request->search_col, 'LIKE', "%" . $request->keyword . "%")
                    ->where('is_approved', 1)
                    ->latest()->paginate(5);
            } elseif ($request->sort_by == 'notapproved') {
                $data = Product::where('user_id', Auth::user()->id)->with('detail')
                    ->where($request->search_col, 'LIKE', "%" . $request->keyword . "%")
                    ->where('is_approved', 0)
                    ->latest()->paginate(5);

            } else {
                $data = Product::where('user_id', Auth::user()->id)->with('detail')
                    ->where($request->search_col, 'LIKE', "%" . $request->keyword . "%")
                    ->latest()->paginate(5);
            }

        }

        // $res['product'] = $data;
        // $res['is_admin'] = auth()->user()->is_admin;

        return json_encode($data);
    }

    public function ApproveProduct($id)
    {
        try {
            $product = Product::find($id);
            if ($product->is_approved == 0) {
                $product->is_approved = 1;
            } else {
                $product->is_approved = 0;
            }
            $product->update();

            $data = Product::where('id', $product->id)->with('detail')
                ->first();

            return $data;
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validation = $request->validate([
            'title' => 'required | string',
            'category' => 'required | string',
            'description' => 'required | string',

            'ad_type' => 'required | string',
            'furnishing' => 'required | string',
            'property_face' => 'required | string',
            'access_road' => 'required | integer',
            'land_area' => 'required | string',
            'contact' => 'required | integer',
            'country' => 'required | string',
            'facilities' => 'required |string',
            'price' => 'required',
            'rooms' => 'required',

            'image' => 'required',
        ]);

        if ($request->hasFile('image')) {
            $image = [];

            foreach ($request->image as $item) {
                $path = Storage::disk('public')->put('products', $item);
                array_push($image, $path);
            }

            // dd(json_encode($image));

            try {
                DB::beginTransaction();
                $product = Product::create([
                    'user_id' => Auth::user()->id,
                    'title' => $request->title,
                    'category' => $request->category,
                    'description' => $request->description,
                    'images' => json_encode($image),
                    'seo_title' => $request->seo_title,
                    'seo_description' => $request->seo_description,
                    'seo_keywords' => $request->seo_keywords,
                    'status' => $request->status,
                ]);

                if (!$product) {
                    throw response()->json($this->reportError('Something went wrong please try again later.'));
                }

                $product_detail = ProductDetail::create([
                    'product_id' => $product->id,
                    'ad_type' => $request->ad_type,
                    'furnishing' => $request->furnishing,
                    'property_face' => $request->property_face,
                    'access_road' => $request->access_road,
                    'land_area' => $request->land_area,
                    'contact' => $request->contact,

                    'country' => $request->country,
                    'locality' => $request->locality,
                    'route' => $request->route,
                    'latitude' => $request->latitude,
                    'longitude' => $request->longitude,

                    'facilities' => $request->facilities,
                    'price' => $request->price,
                    'rooms' => $request->rooms,
                ]);

                if (!$product_detail) {
                    throw response()->json($this->reportError('Something went wrong please try again later.'));
                } else {
                    DB::commit();
                    return response()->json($this->reportSuccess('Product Uploaded Successfully !!'));
                }

            } catch (\Throwable $th) {
                throw $th;
            }
        } else {
            throw response()->json($this->reportError('Something went wrong please try again later.'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());
        $validation = $request->validate([
            'title' => 'required | string',
            'category' => 'required | string',
            'description' => 'required | string',
            'id' => 'required',

            'ad_type' => 'required | string',
            'furnishing' => 'required | string',
            'property_face' => 'required | string',
            'access_road' => 'required | integer',
            'land_area' => 'required | string',
            'contact' => 'required | integer',

            'country' => 'required | string',

            'facilities' => 'required |string',
            'price' => 'required',
            'image' => 'required',
            'rooms' => 'required',
        ]);

        // dd(json_encode($image));

        try {

            $product = Product::find($request->id);
            $prev_images = json_decode($product->images);
            $image = [];

            foreach ($request->image as $key => $item) {

                if (!in_array($item, $prev_images)) {
                    if (gettype($item) == 'object') {
                        $path = Storage::disk('public')->put('products', $item);
                        array_push($image, $path);
                    } else {
                        Storage::delete($item);
                    }
                } else {
                    array_push($image, $item);
                }
            }

            // dd($image);

            $product->title = $request->title;
            $product->category = $request->category;
            $product->description = $request->description;
            $product->images = json_encode($image);
            $product->seo_title = $request->seo_title;
            $product->seo_description = $request->seo_description;
            $product->seo_keywords = $request->seo_keywords;
            $product->status = $request->status;
            $product->update();

            if (!$product) {
                throw response()->json($this->reportError('Something went wrong please try again later.'));
            }

            $product_detail = ProductDetail::where('product_id', $product->id)->update([

                'product_id' => $product->id,
                'ad_type' => $request->ad_type,
                'furnishing' => $request->furnishing,
                'property_face' => $request->property_face,
                'access_road' => $request->access_road,
                'land_area' => $request->land_area,
                'contact' => $request->contact,

                'country' => $request->country,
                'locality' => $request->locality,
                'route' => $request->route,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,

                'facilities' => $request->facilities,
                'price' => $request->price,
                'rooms' => $request->rooms,

            ]);

            if (!$product_detail) {
                throw response()->json($this->reportError('Something went wrong please try again later.'));
            } else {
                DB::commit();
                return response()->json($this->reportSuccess('Product Updated Successfully !!'));
            }

        } catch (\Throwable $th) {
            throw $th;
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            // return true;
            $delete = Product::find($id);
            foreach (json_decode($delete->images) as $value) {
                Storage::delete($value);
            }
            $delete->delete();
            return $delete;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}