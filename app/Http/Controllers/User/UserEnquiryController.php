<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\ReportUser;
use Illuminate\Http\Request;

class UserEnquiryController extends Controller
{
    public function index(Request $request)
    {
        return view('user.enquires');
    }

    public function getAllInquires(Request $request)
    {

        // dd($request);
        if ($request->sort_by == 'reported') {
            $data = InvoiceDetail::join('invoices', 'invoice_details.invoice_id', 'invoices.id')
                ->where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->join('products', 'products.id', 'invoices.product_id')
                ->where('products.user_id', auth()->user()->id)
                ->where('invoices.status', 1)
                ->where('invoices.is_reported', 1)
                ->select('invoice_details.name', 'invoices.is_reported', 'invoice_details.id as d_id', 'invoices.id as i_id', 'invoice_details.email', 'invoice_details.phone_number', 'products.title', 'products.description', 'products.images', 'invoices.created_at')
                ->paginate(10);

        } elseif ($request->sort_by == 'notreported') {
            $data = InvoiceDetail::join('invoices', 'invoice_details.invoice_id', 'invoices.id')
                ->where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->join('products', 'products.id', 'invoices.product_id')
                ->where('products.user_id', auth()->user()->id)
                ->where('invoices.status', 1)
                ->where('invoices.is_reported', 0)
                ->select('invoice_details.name', 'invoices.is_reported', 'invoice_details.id as d_id', 'invoices.id as i_id', 'invoice_details.email', 'invoice_details.phone_number', 'products.title', 'products.description', 'products.images', 'invoices.created_at')
                ->paginate(10);

        } else {
            $data = InvoiceDetail::join('invoices', 'invoice_details.invoice_id', 'invoices.id')
                ->where($request->searchBy, 'LIKE', '%' . $request->keyword . '%')
                ->join('products', 'products.id', 'invoices.product_id')
                ->where('products.user_id', auth()->user()->id)
                ->where('invoices.status', 1)
                ->select('invoice_details.name', 'invoices.is_reported', 'invoice_details.id as d_id', 'invoices.id as i_id', 'invoice_details.email', 'invoice_details.phone_number', 'products.title', 'products.description', 'products.images', 'invoices.created_at')
                ->paginate(10);
        }

        return json_encode($data);

    }

    public function reportUser(Request $request)
    {
        // dd($request->all());
        try {
            $report = ReportUser::create([
                'invoice_id' => $request->user['i_id'],
                'invoice_detail_id' => $request->user['d_id'],
                'message' => $request->message,
                'phone_number' => $request->user['phone_number'],
            ]);
            Invoice::find($request->user['i_id'])->update(['is_reported' => 1]);

            $data = InvoiceDetail::where('invoices.id', $request->user['i_id'])
                ->join('invoices', 'invoice_details.invoice_id', 'invoices.id')
                ->join('products', 'products.id', 'invoices.product_id')
                ->select('invoice_details.name', 'invoices.is_reported', 'invoice_details.id as d_id', 'invoices.id as i_id', 'invoice_details.email', 'invoice_details.phone_number', 'products.title', 'products.description', 'products.images', 'invoices.created_at')
                ->first();

            return $data;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}