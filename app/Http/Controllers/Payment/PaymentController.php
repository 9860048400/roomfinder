<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Jobs\SendInfoMail;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Product;
use App\Repos\Khalti\KhaltiPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PaymentController extends Controller
{
    public $khalti;
    public function __construct()
    {
        $this->khalti = new KhaltiPayment();
    }

    public function verifyKhaltiPayment(Request $request)
    {

        try {
            $response = $this->khalti->makeKhaltiPayment($request->response);
            if ($response['response'] && $response['status'] == 200) {
                if ($request->session()->has('payment_data')) {
                    $pay_data = json_decode($request->session()->get('payment_data'));
                    $invoice = Invoice::create([
                        'reference_id' => "REF" . rand(1000000, 10000000),
                        'price' => config('services.service_charge'),
                        'status' => true,
                        'product_id' => $pay_data->property_id,
                        'response' => json_encode($response),
                        'payment_type' => $pay_data->payment_method,
                    ]);
                    $invoice_detail = InvoiceDetail::create([
                        'invoice_id' => $invoice->id,
                        'name' => $pay_data->name,
                        'email' => $pay_data->email,
                        'ip_address' => $this->getUserIpAddr(),
                        'name' => $pay_data->name,
                        'phone_number' => $pay_data->phone_number,
                        'country_code' => '+977',
                        'browser_data' => isset($pay_data->device_data->browser_data) ? json_encode($pay_data->device_data->browser_data) : null,
                        'fingerprint' => isset($pay_data->device_data->fingerprint) ? $pay_data->device_data->fingerprint : null,
                        'user_agent' => isset($pay_data->device_data->user_agent) ? $pay_data->device_data->user_agent : null,
                        'browser' => isset($pay_data->device_data->browser) ? $pay_data->device_data->browser : null,
                        'os' => isset($pay_data->device_data->os) ? $pay_data->device_data->os : null,
                        'device' => isset($pay_data->device_data->device) ? $pay_data->device_data->device : null,
                        'device_type' => isset($pay_data->device_data->device_type) ? $pay_data->device_data->device_type : null,
                        'is_mobile' => isset($pay_data->device_data->is_mobile) ? $pay_data->device_data->is_mobile : null,
                        'time_zone' => isset($pay_data->device_data->time_zone) ? $pay_data->device_data->time_zone : null,
                        'langugage' => isset($pay_data->device_data->langugage) ? $pay_data->device_data->langugage : null,
                    ]);

                    $data = [
                        'product' => Product::where('id', $pay_data->property_id)->with('detail', 'user')->first(),
                        'invoice' => Invoice::where('id', $invoice->id)->with('details')->first(),
                    ];

                    dispatch(new SendInfoMail($data));

                    return response()->json(['status' => true]);

                } else {
                    return response()->json(['status' => false]);
                }
            } else {
                return response()->json(['status' => false]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

    }

    public function savePaymentSession(Request $request)
    {
        $request->session()->put('payment_data', json_encode($request->data));
    }

    protected function getUserIpAddr()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } else if (isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }
}