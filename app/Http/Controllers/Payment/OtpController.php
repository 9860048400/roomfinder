<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\UserPhoneNumber;
use Illuminate\Http\Request;
use Twilio\Rest\Client;

class OtpController extends Controller
{
    public function sendOTP(Request $request)
    {
        // dd(env('TWILIO_NUMBER'));
        $validation = $request->validate([
            'phone_number' => 'required|string',
        ]);
        try {

            $code = $this->generateToken();
            $this->sendMessage($code, '9860048400');
            $save = UserPhoneNumber::create([
                'phone_number' => $request->phone_number,
                'country_code' => '+977',
                'code' => $code,
            ]);
            return response()->json(['status' => true]);

        } catch (\Throwable $th) {
            throw $th;
        }

    }

    public function verifyOTP(Request $request)
    {
        $validation = $request->validate([
            'phone_number' => 'required|string',
            'OTP' => 'required|string',
        ]);

        try {
            $get = UserPhoneNumber::where('phone_number', $request->phone_number)
                ->latest()->first();

            if ($get && isset($get->code) && $get->code == $request->OTP) {
                $get->update([
                    'is_verified' => 1,
                ]);
                return response()->json(['status' => true]);
            } else {
                return response()->json(['status' => false]);
            }

        } catch (\Throwable $th) {
            throw $th;
        }

    }

    protected function sendMessage($code, $phone)
    {
        $twilio_number = config('services.twilo.number');
        $sid = config('services.twilo.sid');
        $phone = '+977' . $phone;

        $token = config('services.twilo.token');

        $client = new Client($sid, $token);
        $client->messages->create(
            // Where to send a text message (your cell phone?)
            $phone,
            array(
                'from' => $twilio_number,
                'body' => 'Your OTP code is ' . $code,
            )
        );
    }
    protected function generateToken()
    {
        return strval(rand(10000, 100000));
    }
}