<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if (auth()->check()) {
            if (auth()->user()->is_admin == 0 && auth()->user()->role_id == 0 || auth()->user()->role_id == 1) {
                if (auth()->user()->is_locked == 0) {
                    return $next($request);
                }
                return redirect('/locked');
            }
            return redirect('/locked');
        } else {
            return redirect('/');
        }

    }
}