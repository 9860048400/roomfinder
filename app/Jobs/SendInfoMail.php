<?php

namespace App\Jobs;

use App\Mail\InfoMail;
use App\Mail\InvoiceMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendInfoMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // dd($this->data['product']);
        try {
            Mail::to($this->data['invoice']['details']->email)->send(new InvoiceMail($this->data['invoice']));
            Mail::to($this->data['invoice']['details']->email)->send(new InfoMail($this->data['product']));
        } catch (\Exception $e) {
            throw $e;
        }
    }
}