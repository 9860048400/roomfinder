<?php
namespace App\Repos\Khalti;

use Illuminate\Support\Facades\Log;

class KhaltiPayment
{
    protected $secret_key;

    public function __construct()
    {
        $this->secret_key = config('services.khalti.secret');
    }

    public function makeKhaltiPayment($payload)
    {
        $args = http_build_query(array(
            'token' => $payload['token'],
            'amount' => $payload['amount'],
        ));

        Log::info($this->secret_key);

        $url = "https://khalti.com/api/v2/payment/verify/";

        # Make the call using API.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $headers = ["Authorization: Key  $this->secret_key"];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Response
        $response = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return ['response' => $response, 'status' => $status_code];

    }
}