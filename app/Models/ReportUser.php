<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportUser extends Model
{
    use HasFactory;

    protected $fillable = ['invoice_id', 'message', 'invoice_detail_id', 'phone_number', 'is_approved'];
}