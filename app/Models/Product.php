<?php

namespace App\Models;

use App\Models\ProductDetail;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'title',
        'category',
        'description',
        'images',
        'video',
        'is_approved',

        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function detail()
    {
        return $this->hasOne(ProductDetail::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function invoice()
    {
        return $this->hasMany(Invoice::class, 'id', 'product_id');
    }
}