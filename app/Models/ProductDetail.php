<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'ad_type',
        'furnishing',
        'property_face',
        'access_road',
        'land_area',
        'contact',

        'country',
        'locality',
        'route',
        'longitude',
        'latitude',

        'facilities',
        'price',
        'rooms',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}