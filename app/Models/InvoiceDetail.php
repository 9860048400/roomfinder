<?php

namespace App\Models;

use App\Models\Invoice;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice_id',
        'name',
        'email',
        'phone_number',
        'country_code',
        'browser_data',
        'fingerprint',
        'ip_address',
        'user_agent',
        'browser',
        'os',
        'device',
        'device_type',
        'is_mobile',
        'time_zone',
        'language',
    ];

    public function invoiceAll()
    {
        return $this->belongsTo(Invoice::class, 'invoice_id');
    }
}