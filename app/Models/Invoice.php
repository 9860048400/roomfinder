<?php

namespace App\Models;

use App\Models\InvoiceDetail;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = ['is_reported', 'product_id', 'user_id', 'reference_id', 'price', 'status', 'response', 'payment_type'];

    public function details()
    {
        return $this->hasOne(InvoiceDetail::class, 'invoice_id');
    }

    public function property()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

}