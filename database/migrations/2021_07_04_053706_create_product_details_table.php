<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

            $table->string('ad_type')->nullable();
            $table->string('furnishing')->nullable();
            $table->string('property_face')->nullable();
            $table->string('access_road')->nullable();
            $table->string('building_area')->nullable();
            $table->string('land_area')->nullable();
            $table->string('floor')->nullable();
            $table->string('rooms')->nullable();

            $table->string('facilities')->nullable();
            $table->string('contact')->nullable();

            $table->string('country')->nullable();
            $table->string('locality')->nullable();
            $table->string('route')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();

            $table->string('price')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}